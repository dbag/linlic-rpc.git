<?php

namespace linlic\JsonRpc;

interface AdmissionAppleServiceInterface
{
    public function formSubmitCallback(array $params):array;
}