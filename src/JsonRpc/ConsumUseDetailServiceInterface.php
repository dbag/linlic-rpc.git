<?php

namespace linlic\JsonRpc;
// 耗材使用明细
interface ConsumUseDetailServiceInterface
{
    /**
     * 功能字段
     * @param $params
     * @return array
     */
    public function getFields($params):array;

    /**
     * 功能配置
     * @param $params
     * @return array
     */
    public function getConfigs($params):array;

}