<?php

namespace linlic\JsonRpc;

interface SupervisionActivityServiceInterface
{
    /**
     * 功能字段
     * @param array $params
     * @return array
     */
    public function supervisionField(array $params):array;
    

}